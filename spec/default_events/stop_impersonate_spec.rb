require 'rails_helper'

RSpec.describe FirefieldEventStore::DefaultEvents::StopImpersonate do
  let(:user) { User.create name: 'Darth'}
  let(:admin) { Admin.create }
  let(:params) { { record: user, actor_id: admin.id, actor_type: 'Admin' } }
  let(:stream) { "user_#{user.id}" }
  let(:event) { EventStoreEvent.unscoped.last }
  let(:time) { Time.zone.parse('2018-12-09 11:12:10') }
  let(:subject) { described_class.call(params) }

  it 'should create EventStoreEvent record' do
    expect{ subject }.to change{ EventStoreEvent.count }.by(1)
  end

  it 'should return record' do
    expect(subject).to eq(user)
  end

  describe 'EventStoreEvent record fields' do
    before do
      Timecop.freeze(time)
      subject
    end

    it { expect(event.stream).to eq(stream) }
    it { expect(event.event_type).to eq('ImpersonateStopped') }

    it 'should have proper metadata' do
      expect(event.metadata.symbolize_keys).to eq({
        user_id: admin.id, user_type: 'Admin', stopped_at: time.as_json })
    end

    it 'should have empty data' do
      expect(event.data).to be_empty
    end
  end

  context 'Event start impersonate exists' do
    let!(:start_impersonate_event) { EventStoreEvent.create(event_type: 'ImpersonateStarted', stream: stream, metadata: { user_id: admin.id, user_type: 'Admin' }) }

    it 'should contain start impersonate event id in metadata' do
      subject

      expect(event.metadata.symbolize_keys[:start_impersonate_event_id]).to eql start_impersonate_event.id
    end
  end

end