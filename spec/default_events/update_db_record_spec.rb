require 'rails_helper'

RSpec.describe FirefieldEventStore::DefaultEvents::UpdateDbRecord do
  subject { described_class.call(params) }

  let(:user) { User.create name: 'Anakin Skywalker', encrypted_password: '2e913e9c3a76d2ff7a2b7073' }
  let(:admin) { Admin.create }
  let(:extra_metadata) { { updated_by: 'Darth Sidious' } }
  let(:params) { { record: user,
    actor_id: admin.id, actor_type: 'Admin',
    extra_metadata: extra_metadata } }
  let(:event) { EventStoreEvent.last }

  before { user.update_attributes!(name: 'Darth Vader', encrypted_password: '33ded506daaa46ceaae98e6a') }

  it 'should create EventStoreEvent record' do
    expect{ subject }.to change{ EventStoreEvent.count }.by(1)
  end

  it 'should return record' do
    expect(subject).to eq(user)
  end

  describe 'EventStoreEvent record fields' do


    before { subject }

    it { expect(event.stream).to eq("user_#{user.id}") }

    it { expect(event.event_type).to eq('UserUpdated') }

    it 'should have proper metadata' do
      expect(event.metadata.symbolize_keys).to eq({
        user_id: admin.id, user_type: 'Admin', updated_by: 'Darth Sidious',
        updated_at: user.updated_at.as_json
      })
    end

    it 'should have proper data' do
      expect(event.data.symbolize_keys).to include({
        name: ['Anakin Skywalker', 'Darth Vader'],
      })
    end
  end

  it 'should sanitize data before log' do
    subject

    ['password', 'password_confirmation', 'encrypted_password', 'utf8', '_method', 'authenticity_token'].each do |key|
      expect(event.data.keys).not_to include key
    end
  end

  describe 'event data is empty' do
    let!(:other_user) { User.create name: 'Anthony' }
    let!(:params) { { record: other_user,
      actor_id: admin.id, actor_type: 'Admin',
      extra_metadata: extra_metadata } }

    it 'should not log event update if data empty' do
      other_user.save

      expect { subject }.not_to change{ EventStoreEvent.count }
    end

  end

end
