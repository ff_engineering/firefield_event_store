require 'firefield_event_store/main_event'

module FirefieldEventStore
  module DefaultEvents
    class UpdateDbRecord < MainEvent
      def call
        return @record if @record.previous_changes.empty?

        actor_setup

        LogEvent.call(
          record_stream,
          "#{@record.class}Updated",
          metadata.merge!(updated_at: @record.try(:updated_at) || Time.zone.now),
          data.merge!(@record.previous_changes)
        )

        @record
      end
    end
  end
end
