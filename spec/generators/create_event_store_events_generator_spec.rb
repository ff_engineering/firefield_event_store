require 'rails_helper'

module FirefieldEventStore
  module Generators
    RSpec.describe CreateEventStoreEventsGenerator, type: :generator do
      root_dir = File.expand_path('../../../../../tmp', __FILE__)
      destination root_dir

      before :all do
        prepare_destination
        run_generator
      end

      it 'creates create_event_store_events migration' do
        migration_file = 
          Dir.glob("#{root_dir}/db/migrate/*create_event_store_events.rb")

        assert_file migration_file[0], 
          /class CreateEventStoreEvents < ActiveRecord::Migration/
      end
    end
  end
end
