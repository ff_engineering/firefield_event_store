require 'firefield_event_store/main_event'

module FirefieldEventStore
  module DefaultEvents
    class CreateDbRecord < MainEvent
      def call
        actor_setup

        LogEvent.call(
          record_stream,
          "#{@record.class}Created",
          metadata.merge!(created_at: @record.try(:created_at) || Time.zone.now),
          data.merge!(@record.as_json)
        )
        @record
      end
    end
  end
end
