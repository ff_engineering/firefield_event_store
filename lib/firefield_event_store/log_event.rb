module FirefieldEventStore
  class LogEvent < Base
    attr_accessor :stream, :event_type, :metadata, :data

    def initialize(stream, event_type, metadata = {}, data = {})
      self.stream = stream
      self.event_type = event_type
      self.metadata = sanitize(metadata)
      self.data = sanitize(data)
    end

    def call
      EventStoreEvent.create!(stream: stream, event_type: event_type, metadata: metadata, data: data)
    end

    private

    def sanitize(data)
      data.reject { |key, _val| key.to_s.in?(
        ['password', 'password_confirmation', 'encrypted_password', 'utf8', '_method', 'authenticity_token']
      ) }
    end
  end
end
