require 'firefield_event_store/main_event'

module FirefieldEventStore
  module DefaultEvents
    class DestroyDbRecord < MainEvent
      def call
        actor_setup

        LogEvent.call(
          record_stream,
          "#{@record.class}Destroyed",
          metadata.merge!(destroyed_at: Time.zone.now),
          data.merge!(@record.as_json)
        )
        @record
      end
    end
  end
end
