module FirefieldEventStore
  module DefaultEvents
    class SignUp < MainEvent
      def call
        @actor = record

        LogEvent.call(
          record_stream,
          "#{@record.class}SignedUp",
          metadata.merge!(signed_up_at: Time.zone.now),
          data.merge!(@record.as_json)
        )
        @record
      end
    end
  end
end
