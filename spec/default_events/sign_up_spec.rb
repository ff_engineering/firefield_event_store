require 'rails_helper'

RSpec.describe FirefieldEventStore::DefaultEvents::SignUp do
  let(:user) { User.create name: 'Darth'}
  let(:params) { { record: user } }
  let(:event) { EventStoreEvent.unscoped.last }
  let(:time) { Time.zone.parse('2018-12-09 11:12:10') }
  let(:subject) { described_class.call(params) }

  it 'should create EventStoreEvent record' do
    expect{ subject }.to change{ EventStoreEvent.count }.by(1)
  end

  it 'should return record' do
    expect(subject).to eq(user)
  end

  describe 'EventStoreEvent record fields' do
    before do
      Timecop.freeze(time)
      subject
    end

    after do
      Timecop.return
    end

    it { expect(event.stream).to eq("user_#{user.id}") }
    it { expect(event.event_type).to eq('UserSignedUp') }

    it 'should have proper metadata' do
      expect(event.metadata.symbolize_keys).to eq({
        user_id: user.id, user_type: 'User', signed_up_at: time.as_json })
    end

    it 'should have proper data' do
      expect(event.data.symbolize_keys).to eq({
        id: user.id, name: 'Darth', created_at: user.created_at.as_json, updated_at: user.updated_at.as_json
      })
    end
  end
end