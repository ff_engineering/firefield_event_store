$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "firefield_event_store/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "firefield_event_store"
  s.version     = FirefieldEventStore::VERSION
  s.authors     = ["Adam Wieczorkowski"]
  s.email       = ["adam.wieczorkowski@naturaily.com"]
  s.homepage    = ""
  s.summary     = "Summary of FirefieldEventStore."
  s.description = "Description of FirefieldEventStore."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", ">= 4.0"

  s.add_development_dependency "pg", "~> 0.18"
  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "pry-byebug"
  s.add_development_dependency "generator_spec"
  s.add_development_dependency "timecop"
end
