module FirefieldEventStore
  module DefaultEvents
    class StopImpersonate < MainEvent
      def call
        actor_setup

        extra_metadata = { stopped_at: Time.zone.now }

        # find event id for impersonate started event
        if start_impersonate_event = EventStoreEvent.
          where(event_type: 'ImpersonateStarted', stream: record_stream).
          where("metadata ->> 'user_type' = '%s' AND metadata ->> 'user_id' = '%s'", actor_type, actor_id).
          last

          extra_metadata[:start_impersonate_event_id] = start_impersonate_event.id
        end

        LogEvent.call(
          record_stream,
          'ImpersonateStopped',
          metadata.merge!(extra_metadata)
        )
        @record
      end
    end
  end
end