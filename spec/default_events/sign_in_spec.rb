require 'rails_helper'

RSpec.describe FirefieldEventStore::DefaultEvents::SignIn do
  let(:user) { User.create name: 'Darth'}
  let(:params) { { record: user } }
  let(:event) { EventStoreEvent.unscoped.last }
  let(:time) { Time.zone.parse('2018-12-09 11:12:10') }
  let(:subject) { described_class.call(params) }

  it 'should create EventStoreEvent record' do
    expect{ subject }.to change{ EventStoreEvent.count }.by(1)
  end

  it 'should return record' do
    expect(subject).to eq(user)
  end

  describe 'EventStoreEvent record fields' do
    before do
      Timecop.freeze(time)
      subject
    end

    after do
      Timecop.return
    end

    it { expect(event.stream).to eq("user_#{user.id}") }
    it { expect(event.event_type).to eq('UserSignedIn') }

    it 'should have proper metadata' do
      expect(event.metadata.symbolize_keys).to eq({
        user_id: user.id, user_type: 'User', signed_in_at: time.as_json })
    end

    it 'should have empty data' do
      expect(event.data).to be_empty
    end
  end
end