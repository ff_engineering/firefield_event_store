require 'rails_helper'

RSpec.describe FirefieldEventStore::DefaultEvents::DestroyDbRecord do
  let(:user) { User.create(name: 'Anthony') }
  let(:admin) { Admin.create }
  let(:extra_metadata) { { deleted_by: 'Darth Sidious' } }
  let(:params) { { record: user,
    actor_id: admin.id, actor_type: 'Admin',
    extra_metadata: extra_metadata } }

  subject do
    described_class.call(params)
  end

  it 'should create EventStoreEvent record' do
    expect{ subject }.to change{ EventStoreEvent.count }.by(1)
  end

  it 'should return record' do
    expect(subject).to eq(user)
  end

  describe 'EventStoreEvent record fields' do
    let(:event) { EventStoreEvent.last }
    let(:time) { Time.zone.parse('2017-03-31 12:42:10') }

    before do
      Timecop.freeze(time)
      subject
    end

    it { expect(event.stream).to eq("user_#{user.id}") }

    it { expect(event.event_type).to eq('UserDestroyed') }

    it 'should have proper metadata' do
      expect(event.metadata.symbolize_keys).to eq({
        user_id: admin.id, user_type: 'Admin', deleted_by: 'Darth Sidious',
        destroyed_at: time.as_json
      })
    end

    it 'should have proper data' do
      expect(event.data.symbolize_keys).to eq({
        id: user.id, name: 'Anthony',
        created_at: user.created_at.as_json, updated_at: user.updated_at.as_json
      })
    end
  end
end
