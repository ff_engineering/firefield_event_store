require 'firefield_event_store/default_events/create_db_record'
require 'firefield_event_store/default_events/destroy_db_record'
require 'firefield_event_store/default_events/update_db_record'

require 'firefield_event_store/default_events/start_impersonate'
require 'firefield_event_store/default_events/stop_impersonate'

require 'firefield_event_store/default_events/sign_up'
require 'firefield_event_store/default_events/sign_in'
require 'firefield_event_store/default_events/sign_out'

module FirefieldEventStore
  module DefaultEvents
  end
end
