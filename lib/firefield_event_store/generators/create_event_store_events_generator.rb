require 'rails/generators'
require 'rails/generators/migration'

module FirefieldEventStore
  module Generators
    class CreateEventStoreEventsGenerator < ::Rails::Generators::Base
      include Rails::Generators::Migration
      source_root File.expand_path('../templates', __FILE__)
      desc 'Create migration with event_store_events table'

      def self.next_migration_number(path)
        next_migration_number = current_migration_number(path) + 1
        ActiveRecord::Migration.next_migration_number(next_migration_number)
      end

      def copy_migrations
        migration_template 'create_event_store_events.rb',
          'db/migrate/create_event_store_events.rb'
      end
    end
  end
end
