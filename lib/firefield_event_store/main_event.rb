require 'firefield_event_store/base'
require 'firefield_event_store/log_event'

module FirefieldEventStore
  class MainEvent < Base
    attr_accessor :record, :actor_id, :actor_type, :extra_metadata, :extra_data

    def initialize(record:, actor_id: nil, actor_type: nil, extra_metadata: {}, extra_data: {})
      self.record = record
      self.actor_id = actor_id
      self.actor_type = actor_type
      self.extra_metadata = extra_metadata
      self.extra_data = extra_data
    end

    protected

    def record_stream
      "#{@record.class.to_s.underscore}_#{@record.id}" if @record
    end

    def actor_setup
      @actor = @actor_id && @actor_type ? @actor_type.constantize.find(@actor_id) : nil
    end

    def data
      @data ||= @extra_data
    end

    def metadata
      @metadata ||= user_data.merge!(@extra_metadata)
    end

    def user_data
      @actor ? { user_id: @actor.id, user_type: @actor.class.name } : {}
    end
  end
end
