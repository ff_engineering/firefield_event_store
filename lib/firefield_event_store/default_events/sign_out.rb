module FirefieldEventStore
  module DefaultEvents
    class SignOut < MainEvent
      def call
        @actor = record

        LogEvent.call(
          record_stream,
          "#{@record.class}SignedOut",
          metadata.merge!(signed_out_at: Time.zone.now)
        )
        @record
      end
    end
  end
end
