require 'rails_helper'

RSpec.describe FirefieldEventStore::DefaultEvents::CreateDbRecord do
  let(:user) { User.create(name: 'Mefisto') }
  let(:admin) { Admin.create }
  let(:extra_metadata) { { created_by: 'Darth Sidious' } }
  let(:extra_data) { { name: 'Mefisto' } }
  let(:params) { { record: user,
    actor_id: admin.id, actor_type: 'Admin',
    extra_metadata: extra_metadata, extra_data: extra_data } }

  subject do
    described_class.call(params)
  end

  it 'should create EventStoreEvent record' do
    expect{ subject }.to change{ EventStoreEvent.count }.by(1)
  end

  it 'should return record' do
    expect(subject).to eq(user)
  end

  describe 'EventStoreEvent record fields' do
    let(:event) { EventStoreEvent.last }

    before { subject }

    it { expect(event.stream).to eq("user_#{user.id}") }

    it { expect(event.event_type).to eq('UserCreated') }

    it 'should have proper metadata' do
      expect(event.metadata.symbolize_keys).to eq({
        user_id: admin.id, user_type: 'Admin', created_by: 'Darth Sidious',
        created_at: user.created_at.as_json
      })
    end

    it 'should have proper data' do
      expect(event.data.symbolize_keys).to eq({
        id: user.id, name: 'Mefisto',
        created_at: user.created_at.as_json, updated_at: user.updated_at.as_json
      })
    end
  end
end
