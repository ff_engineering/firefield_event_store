module FirefieldEventStore
  module DefaultEvents
    class StartImpersonate < MainEvent
      def call
        actor_setup

        LogEvent.call(
          record_stream,
          'ImpersonateStarted',
          metadata.merge!(started_at: Time.zone.now)
        )
        @record
      end
    end
  end
end