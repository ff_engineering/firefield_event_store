module FirefieldEventStore
  module DefaultEvents
    class SignIn < MainEvent
      def call
        @actor = record

        LogEvent.call(
          record_stream,
          "#{@record.class}SignedIn",
          metadata.merge!(signed_in_at: Time.zone.now)
        )
        @record
      end
    end
  end
end
