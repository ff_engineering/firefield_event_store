require 'rails_helper'

module FirefieldEventStore
  module Generators
    RSpec.describe EnableUuidExtensionGenerator, type: :generator do
      root_dir = File.expand_path('../../../../../tmp', __FILE__)
      destination root_dir

      before :all do
        prepare_destination
        run_generator
      end

      it 'creates enable_uuid_extension migration' do
        migration_file = 
          Dir.glob("#{root_dir}/db/migrate/*enable_uuid_extension.rb")

        assert_file migration_file[0], 
          /class EnableUuidExtension < ActiveRecord::Migration/
      end
    end
  end
end
