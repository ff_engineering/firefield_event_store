require 'rails/generators'
require 'rails/generators/migration'

module FirefieldEventStore
  module Generators
    class EnableUuidExtensionGenerator < ::Rails::Generators::Base
      include Rails::Generators::Migration
      source_root File.expand_path('../templates', __FILE__)
      desc 'Add the migration for UUID extension'

      def self.next_migration_number(path)
        next_migration_number = current_migration_number(path) + 1
        ActiveRecord::Migration.next_migration_number(next_migration_number)
      end

      def copy_migrations
        migration_template 'enable_uuid_extension.rb',
          'db/migrate/enable_uuid_extension.rb'
      end
    end
  end
end
