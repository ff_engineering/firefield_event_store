# FirefieldEventStore

Log arbitrary events and metadata in your Rails application, using ActiveRecord
and Postgresql.

# Overview

This library provides a predefined collection of service objects you can use to
log different types of events.

Custom services can be created to create arbitrary events that make sense for
your application.

# Getting started

## Prerequisites

This library only works with Postgresql.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'firefield_event_store'
```

And then execute:

```bash
$ bundle install
```

After that, generate migrations:

```
bin/rails g migration firefield_event_store:enable_uuid_extension
```

and

```
bin/rails g firefield_event_store:create_event_store_events
```

Then run:

```
bin/rake db:migrate
```

Also in `app/models/` add `event_store_event.rb`:

```ruby
class EventStoreEvent < ApplicationRecord
end
```

# Usage

```ruby
FirefieldEventStore::DefaultEvents::CreateDbRecord.call(record, user, extra_metadata, extra_data)
```

## Arguments:

* `record (object)` - Record related to event
* `user(object, optional)` - User who trigerred event
* `extra_metadata(hash, optional)` - Extra metadata
* `extra_data(hash, optional)` - Extra data.

## Default Events

### CreateDbRecord

Example:

```ruby
FirefieldEventStore::DefaultEvents::CreateDbRecord.call(post, user)
```

```ruby
#<EventStoreEvent:0x007fa6c3db6bb0
  id: 1,
  stream: "post_1",
  event_type: "PostCreated",
  event_id: "2adec400-085c-4b91-a9a2-e3511b8f77cc",
  metadata: {
    "user_id"=>42,
    "user_type"=>"User",
    "created_at"=>"2017-04-03T07:03:03.536Z"
  },
  data: {
    "id"=>1,
    "title"=>"Great post about events",
    "text"=>"Melius salutandi mnesarchum no quo...",
    "category"=>"News"
    "created_at"=>"2017-04-03T07:03:03.536Z",
    "updated_at"=>"2017-04-03T07:03:03.536Z",
  },
  created_at: Mon, 03 Apr 2017 07:03:03 UTC +00:00
>
```

### UpdateDbRecord

Example:

```ruby
FirefieldEventStore::DefaultEvents::UpdateDbRecord.call(post, user)
```

```ruby
#<EventStoreEvent:0x007fa5f183afd8
  id: 2,
  stream: "post_1",
  event_type: "PostUpdated",
  event_id: "a5f85ca9-adcc-4782-8c35-c75be4d431d1",
  metadata: {
    "user_id"=>42,
    "user_type"=>"User",
    "updated_at"=>"2017-04-03T07:26:25.720Z"
  },
  data: {
    "title"=>["Great post about events", "Events Tutorial"],
    "category"=>["News", "Tutorials"],
    "updated_at"=>["2017-04-03T07:03:03.536Z", "2017-04-03T07:26:25.720Z"]
  },
  created_at: Mon, 03 Apr 2017 07:26:25 UTC +00:00
>
```

### DestroyDbRecord

Example:

```ruby
FirefieldEventStore::DefaultEvents::DestroyDbRecord.call(post, admin, { reason: "Stolen content" })
```

```ruby
#<EventStoreEvent:0x007fdfda0a18f8
  id: 3,
  stream: "post_1",
  event_type: "PostDestroyed",
  event_id: "47c54d8d-d98e-4794-9c4b-19d0f6e3996e",
  metadata: {
    "reason"=>"Stolen content",
    "user_id"=>6,
    "user_type"=>"Admin",
    "destroyed_at"=>"2017-04-03T08:22:26.731Z"
  },
  data: {},
  created_at: Mon, 03 Apr 2017 08:22:26 UTC +00:00
>
```

## Custom Events

Your custom event class should look like this (put it in `app/events/`):

```ruby
class MyCustomEvent < FirefieldEventStore::MainEvent
  def call
    LogEvent.call(stream_name, event_name, metadata, data)
    object
  end
end
```

### `MainEvent` class provides:

* `object` - record passed when event is called
* `user` - user passed when event is called (default = `nil`)
* `metadata` - extra_metadata passed when event is called + user data if user provided (default = '{}')
* `data` - extra_data passed when event is called (default = '{}')

### `LogEvent` arguments:

* `stream_name` - `string`
* `event_name` - `string`
* `metedata` - `json`
* `data` - `json`

### Call it:

```
MyCustomEvent.call(record, user, extra_metadata, extra_data)
```

```ruby
FirefieldEventStore::DefaultEvents::CreateDbRecord.call(record, user, extra_metadata, extra_data)
```

# Contributing

## Tests

### DB setup

First you need to create database for your tests. Login to your psql console using command _psql_. Then create database:

```
CREATE DATABASE firefield_event_store_test;
```

### Running specs

```
bundle exec rspec
```

# License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
