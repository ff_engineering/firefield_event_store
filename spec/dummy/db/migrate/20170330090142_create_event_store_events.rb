class CreateEventStoreEvents < ActiveRecord::Migration
  def change
    create_table :event_store_events, force: true do |t|
      t.string :stream, null: false, index: true
      t.string :event_type, null: false
      t.uuid :event_id, null: false, default: 'uuid_generate_v4()'
      t.jsonb :metadata, default: '{}'
      t.jsonb :data, null: false, default: '{}'
      t.datetime :created_at, null: false
    end

    add_index :event_store_events, :event_id, unique: true
  end
end
